package com.besaba.okomaru_server.junT58.NoUsePotAndExpBin;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{

	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onUse(PlayerInteractEvent event){
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.LEFT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_AIR)){
			if(event.getPlayer().getItemInHand() != null){
				Material hand = event.getPlayer().getItemInHand().getType();
				if(hand.equals(Material.POTION) || hand.equals(Material.EXP_BOTTLE)){
					if(event.getPlayer().getGameMode() != GameMode.CREATIVE){
						event.setCancelled(true);
						event.getPlayer().updateInventory();
					}
				}
			}
		}
	}

}
